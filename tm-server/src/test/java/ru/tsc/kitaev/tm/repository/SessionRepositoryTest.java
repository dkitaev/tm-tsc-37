package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

public class SessionRepositoryTest {

    @NotNull
    private final Connection connection;

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    @NotNull
    private final String userId;

    public SessionRepositoryTest() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        userRepository = new UserRepository(connection);
        @NotNull final User user = new User();
        user.setLogin("test");
        userId = user.getId();
        @NotNull final String password = "test";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
        userRepository.add(user);
        connection.commit();
        sessionRepository = new SessionRepository(connection);
        session = new Session();
        sessionId = session.getId();
        session.setUserId(userId);
        session.setTimestamp(System.currentTimeMillis());
    }

    @Before
    public void before() {
    }

    @Test
    public void openCloseTest() throws SQLException {
        sessionRepository.add(session);
        connection.commit();
        @NotNull final  Session tempSession = sessionRepository.findById(sessionId);
        Assert.assertEquals(session.getId(), tempSession.getId());
        Assert.assertEquals(session.getUserId(), tempSession.getUserId());
        Assert.assertEquals(session.getSignature(), tempSession.getSignature());
        sessionRepository.remove(session);
        connection.commit();
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void existsTest() throws SQLException {
        sessionRepository.add(session);
        connection.commit();
        Assert.assertTrue(sessionRepository.exists(sessionId));
    }

    @After
    public void after() throws SQLException {
        sessionRepository.clear();
        userRepository.removeById(session.getUserId());
        connection.commit();
    }

}
