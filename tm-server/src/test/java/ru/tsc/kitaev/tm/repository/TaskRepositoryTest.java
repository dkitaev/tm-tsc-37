package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class TaskRepositoryTest {

    @NotNull
    private final Connection connection;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final Task task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTaskDescription";

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final static String PROJECT_NAME = "testProject";

    @NotNull
    private final String userId;

    public TaskRepositoryTest() throws SQLException {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        taskRepository = new TaskRepository(connection);
        userRepository = new UserRepository(connection);
        projectRepository = new ProjectRepository(connection);
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("test");
        user.setPasswordHash(HashUtil.salt("test", 5, "test"));
        userRepository.add(user);
        task = new Task();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
        project.setName(PROJECT_NAME);
        connection.commit();
    }

    @Before
    public void before() throws SQLException {
        taskRepository.add(userId, task);
        projectRepository.add(userId, project);
        connection.commit();
    }

    @Test
    public void findProjectTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskName);
        Assert.assertEquals(task, taskRepository.findById(userId, taskId));
        Assert.assertEquals(task, taskRepository.findByIndex(userId, 0));
        Assert.assertEquals(task, taskRepository.findByName(userId, taskName));
    }

    @Test
    public void removeTaskByIdTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.removeById(userId, taskId);
        connection.commit();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void removeTaskByIndexTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        taskRepository.removeByIndex(userId, 0);
        connection.commit();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void removeTaskByNameTest() throws SQLException {
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.removeByName(userId, taskName);
        connection.commit();
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void updateByIdTest() throws SQLException {
        @NotNull final String newName = "newTestTask";
        @NotNull final String newDescription = "newTestTaskDescription";
        taskRepository.updateById(userId, taskId, newName, newDescription);
        connection.commit();
        Assert.assertEquals(newName, taskRepository.findById(userId, projectId).getName());
        Assert.assertEquals(newDescription, taskRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(taskName, taskRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(taskDescription, taskRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void updateByIndexTest() throws SQLException {
        @NotNull final String newName = "newTestTask";
        @NotNull final String newDescription = "newTestTaskDescription";
        taskRepository.updateByIndex(userId, 0, newName, newDescription);
        connection.commit();
        Assert.assertEquals(newName, taskRepository.findById(userId, projectId).getName());
        Assert.assertEquals(newDescription, taskRepository.findById(userId, projectId).getDescription());
        Assert.assertNotEquals(taskName, taskRepository.findById(userId, projectId).getName());
        Assert.assertNotEquals(taskDescription, taskRepository.findById(userId, projectId).getDescription());
    }

    @Test
    public void startByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.startById(userId, taskId);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void startByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        taskRepository.startByIndex(userId, 0);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void startByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.startByName(userId, taskName);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void finishByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.finishById(userId, taskId);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void finishByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        taskRepository.finishByIndex(userId, 0);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void finishByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.finishByName(userId, taskName);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void changeStatusByIdTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskId);
        taskRepository.changeStatusById(userId, taskId, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findById(userId, taskId).getStatus());
        taskRepository.changeStatusById(userId, taskId, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findById(userId, taskId).getStatus());
        taskRepository.changeStatusById(userId, taskId, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findById(userId, taskId).getStatus());
    }

    @Test
    public void changeStatusByIndexTest() throws SQLException {
        Assert.assertNotNull(userId);
        taskRepository.changeStatusByIndex(userId, 0, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByIndex(userId, 0).getStatus());
        taskRepository.changeStatusByIndex(userId, 0, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByIndex(userId, 0).getStatus());
        taskRepository.changeStatusByIndex(userId, 0, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByIndex(userId, 0).getStatus());
    }

    @Test
    public void changeStatusByNameTest() throws SQLException {
        Assert.assertNotNull(userId);
        Assert.assertNotNull(taskName);
        taskRepository.changeStatusByName(userId, taskName, Status.IN_PROGRESS);
        connection.commit();
        Assert.assertEquals(Status.IN_PROGRESS, taskRepository.findByName(userId, taskName).getStatus());
        taskRepository.changeStatusByName(userId, taskName, Status.COMPLETED);
        connection.commit();
        Assert.assertEquals(Status.COMPLETED, taskRepository.findByName(userId, taskName).getStatus());
        taskRepository.changeStatusByName(userId, taskName, Status.NOT_STARTED);
        connection.commit();
        Assert.assertEquals(Status.NOT_STARTED, taskRepository.findByName(userId, taskName).getStatus());
    }

    @Test
    public void findAllTaskByProjectIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        connection.commit();
        Assert.assertEquals(task, taskRepository.findAllTaskByProjectId(userId, projectId).get(0));
    }

    @Test
    public void bindTaskToProjectByIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        connection.commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void unbindTaskByIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        connection.commit();
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
        taskRepository.unbindTaskById(userId, projectId, taskId);
        connection.commit();
        Assert.assertNull(taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void removeAllTaskByProjectIdTest() throws SQLException {
        Assert.assertNotNull(project);
        Assert.assertNotNull(task);
        Assert.assertNotNull(userId);
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final Task task1 = new Task();
        task1.setName("testTask1");
        task1.setUserId(userId);
        taskRepository.add(userId, task1);
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        connection.commit();
        Assert.assertEquals(2, taskRepository.findAllTaskByProjectId(userId, projectId).size());
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        connection.commit();
        Assert.assertEquals(0, taskRepository.findAllTaskByProjectId(userId, projectId).size());
    }

    @After
    public void after() throws SQLException {
        taskRepository.clear(userId);
        projectRepository.clear(userId);
        userRepository.removeById(userId);
        connection.commit();
        connection.close();
    }

}
