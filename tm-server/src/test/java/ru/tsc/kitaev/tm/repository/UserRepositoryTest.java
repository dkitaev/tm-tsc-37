package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.service.ConnectionService;
import ru.tsc.kitaev.tm.service.PropertyService;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public class UserRepositoryTest {

    @NotNull
    private final Connection connection;

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userTest";

    @NotNull
    private final String userEmail = "userTest@mail.com";

    public UserRepositoryTest() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final IConnectionService connectionService = new ConnectionService(propertyService);
        connection = connectionService.getConnection();
        userRepository = new UserRepository(connection);
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        user.setEmail(userEmail);
        @NotNull final String password = "userTest";
        @NotNull final String secret = propertyService.getPasswordSecret();
        final int iteration = propertyService.getPasswordIteration();
        user.setPasswordHash(HashUtil.salt(secret, iteration, password));
    }

    @Before
    public void before() throws SQLException {
        userRepository.add(user);
        connection.commit();
    }

    @Test
    public void findByUserTest() throws SQLException {
        Assert.assertEquals(user, userRepository.findById(userId));
        Assert.assertEquals(user, userRepository.findByIndex(0));
        Assert.assertEquals(user, userRepository.findByLogin(userLogin));
        Assert.assertEquals(user, userRepository.findByEmail(userEmail));
    }

    @Test
    public void removeUserTest() throws SQLException {
        userRepository.removeUser(user);
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void removeByLoginTest() throws SQLException {
        userRepository.removeByLogin(userLogin);
        connection.commit();
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @After
    public void after() throws SQLException {
        userRepository.removeById(userId);
        connection.commit();
    }

}
