package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @NotNull
    private final static String TASK_TABLE = "tasks";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return TASK_TABLE;
    }

    @NotNull
    @Override
    protected Task fetch(@NotNull ResultSet row) throws SQLException {
      @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setUserId(row.getString("user_id"));
        task.setStatus(Status.valueOf(row.getString("status")));
        task.setCreated(row.getTimestamp("created"));
        task.setStartDate(row.getTimestamp("start_date"));
        task.setProjectId(row.getString("project_id"));
        return task;
    }

    @Override
    public void add(@NotNull String userId, @NotNull Task task) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, name, description, user_id, status, created, start_date, project_id)" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, task.getId());
        statement.setString(2, task.getName());
        statement.setString(3, task.getDescription());
        statement.setString(4, task.getUserId());
        statement.setString(5, task.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(task.getCreated().getTime()));
        @Nullable final Date startDate = task.getStartDate();
        statement.setTimestamp(7, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.setString(8, task.getProjectId());
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void addAll(@NotNull List<Task> tasks) throws SQLException {
        for (Task task : tasks) {
            add(task.getUserId(), task);
        }
    }

    @NotNull
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final Task result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET name = ?, description = ?" +
                " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException {
        @NotNull final String taskId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET name = ?, description = ?" +
                " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start date = ?" +
                " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String taskId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start date = ?" +
                " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start date = ?" +
                " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String taskId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    ) throws SQLException {
        @NotNull final String taskId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void changeStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<Task> findAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        if (result.isEmpty()) throw new TaskNotFoundException();
        return result;
    }

    @Override
    public void bindTaskToProjectById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET project_id = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, projectId);
        statement.setString(2, userId);
        statement.setString(3, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void unbindTaskById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() +
                " SET project_id = ? WHERE project_id = ? AND user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setNull(1, Types.VARCHAR);
        statement.setString(2, projectId);
        statement.setString(3, userId);
        statement.setString(4, taskId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeAllTaskByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND project_id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, projectId);
        statement.executeUpdate();
        statement.close();
    }

}
