package ru.tsc.kitaev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectEndpoint {

    @SneakyThrows
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "entity", partName = "entity") @Nullable final Project entity
    );

    @SneakyThrows
    @WebMethod
    public void clearProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @NotNull
    @SneakyThrows
    @WebMethod
    public List<Project> findProjectAll(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @NotNull
    @SneakyThrows
    @WebMethod
    public List<Project> findProjectAllSorted(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "sort", partName = "sort") @Nullable final String sort
    );

    @Nullable
    @SneakyThrows
    @WebMethod
    public Project findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @NotNull
    @SneakyThrows
    @WebMethod
    public Project findProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @SneakyThrows
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @WebMethod
    @SneakyThrows
    public void removeProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @NotNull
    @WebMethod
    @SneakyThrows
    public Project createProject(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    );

    @NotNull
    @WebMethod
    @SneakyThrows
    public Project findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    );

    @WebMethod
    @SneakyThrows
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    );

    @WebMethod
    @SneakyThrows
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    );

    @WebMethod
    @SneakyThrows
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @NotNull final String description
    );

    @WebMethod
    @SneakyThrows
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @WebMethod
    @SneakyThrows
    public void startProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @WebMethod
    @SneakyThrows
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    );

    @WebMethod
    @SneakyThrows
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @WebMethod
    @SneakyThrows
    public void finishProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @WebMethod
    @SneakyThrows
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    );

    @WebMethod
    @SneakyThrows
    public void changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    );

    @WebMethod
    public void changeProjectStatusByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    );

    @WebMethod
    @SneakyThrows
    public void changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    );

    @WebMethod
    @SneakyThrows
    public boolean existsProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    );

    @WebMethod
    @SneakyThrows
    public boolean existsProjectByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") final int index
    );

}
