package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.model.User;

import java.sql.SQLException;
import java.util.List;

public interface IUserService extends IService<User> {

    void addAll(@NotNull List<User> users) throws SQLException;

    @NotNull
    User findByLogin(@Nullable String login) throws SQLException;

    void isLoginExists(@Nullable String login) throws SQLException;

    @NotNull
    User findByEmail(@Nullable String email) throws SQLException;

    void isEmailExists(@Nullable String email) throws SQLException;

    void removeUser(@Nullable User user) throws SQLException;

    void removeByLogin(@Nullable String login) throws SQLException;

    @NotNull
    User create(@Nullable String login, @Nullable String password) throws SQLException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email) throws SQLException;

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable Role role) throws SQLException;

    void setPassword(@Nullable String userId, @Nullable String password) throws SQLException;

    void setPassword(@NotNull User user, @NotNull String password);

    void updateUser(@Nullable String userId, @NotNull String firstName, @NotNull String lastName, @NotNull String middleName) throws SQLException;

    void lockUserByLogin(@Nullable String login) throws SQLException;

    void unlockUserByLogin(@Nullable String login) throws SQLException;

}
