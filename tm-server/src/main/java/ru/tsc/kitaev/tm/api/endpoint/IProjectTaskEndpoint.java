package ru.tsc.kitaev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IProjectTaskEndpoint {

    @NotNull
    @SneakyThrows
    @WebMethod
    List<Task> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    );

    @WebMethod
    @SneakyThrows
    void bindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    );

    @WebMethod
    @SneakyThrows
    void unbindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    );

    @WebMethod
    @SneakyThrows
    void removeById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    );

    @WebMethod
    @SneakyThrows
    void removeByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    );

    @WebMethod
    @SneakyThrows
    void removeByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "nam", partName = "nam") @Nullable final String name
    );

}
