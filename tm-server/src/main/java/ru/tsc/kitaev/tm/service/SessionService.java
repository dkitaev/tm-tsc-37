package ru.tsc.kitaev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.ISessionRepository;
import ru.tsc.kitaev.tm.api.service.*;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.exception.user.AccessDeniedException;
import ru.tsc.kitaev.tm.exception.user.UserIsLockedException;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.repository.SessionRepository;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService,
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(connectionService, logService);
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @NotNull
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) throws SQLException {
        @Nullable final User user = checkDataAccess(login, password);
        if (user == null) throw new AccessDeniedException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ISessionRepository sessionRepository = getRepository(connection);
        try {
            sessionRepository.add(session);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
        return sign(session);
    }

    @Nullable
    @Override
    public User checkDataAccess(@Nullable final String login, @Nullable final String password) throws SQLException {
        if (login == null || login.isEmpty()) return null;
        if (password == null || password.isEmpty()) return null;
        @Nullable final User user = serviceLocator.getUserService().findByLogin(login);
        @NotNull final String secret = serviceLocator.getPropertyService().getPasswordSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        if (user.getLocked()) throw new UserIsLockedException();
        if (user.getPasswordHash().equals(hash)) {
            return user;
        } else {
            return null;
        }
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final Session session, final Role role) {
        if (session == null) throw new AccessDeniedException();
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException();
        @Nullable final User user = serviceLocator.getUserService().findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session) throws SQLException {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp).getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ISessionRepository sessionRepository = getRepository(connection);
        try {
            if (!sessionRepository.exists(session.getId())) throw new AccessDeniedException();
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String secret = serviceLocator.getPropertyService().getSessionSecret();
        @NotNull final Integer iteration = serviceLocator.getPropertyService().getSessionIteration();
        @Nullable final String signature = HashUtil.sign(secret, iteration, session);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void close(@Nullable Session session) throws SQLException {
        if (session == null) return;
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ISessionRepository sessionRepository = getRepository(connection);
        try {
            sessionRepository.remove(session);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

}
