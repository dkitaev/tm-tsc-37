package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.model.User;

import java.sql.*;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    private final static String PROJECT_TABLE = "projects";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    protected String getTableName() {
        return PROJECT_TABLE;
    }

    @NotNull
    @Override
    protected Project fetch(@NotNull ResultSet row) throws SQLException {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setUserId(row.getString("user_id"));
        project.setStatus(Status.valueOf(row.getString("status")));
        project.setCreated(row.getTimestamp("created"));
        project.setStartDate(row.getTimestamp("start_date"));
        return project;
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final Project project) throws SQLException {
        @NotNull final String query = "INSERT INTO " + getTableName() +
                " (id, name, description, user_id, status, created, start_date) VALUES (?, ?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, project.getId());
        statement.setString(2, project.getName());
        statement.setString(3, project.getDescription());
        statement.setString(4, project.getUserId());
        statement.setString(5, project.getStatus().toString());
        statement.setTimestamp(6, new Timestamp(project.getCreated().getTime()));
        @Nullable final Date startDate = project.getStartDate();
        statement.setTimestamp(7, startDate == null ? null : new Timestamp(startDate.getTime()));
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void addAll(@NotNull List<Project> projects) throws SQLException {
        for (Project project : projects) {
            add(project.getUserId(), project);
        }
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final Project result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET name = ?, description = ?" +
                " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException {
        @NotNull final String projectId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET name = ?, description = ?" +
                " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, name);
        statement.setString(2, description);
        statement.setString(3, userId);
        statement.setString(4, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start date = ?" +
                " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String projectId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start date = ?" +
                " WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ?, start date = ?" +
                " WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.IN_PROGRESS.toString());
        statement.setTimestamp(2, new Timestamp(new Date().getTime()));
        statement.setString(3, userId);
        statement.setString(4, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishById(@NotNull final String userId, @NotNull final String id) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException {
        @NotNull final String projectId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void finishByName(@NotNull final String userId, @NotNull final String name) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? AND name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, Status.COMPLETED.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? and id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    ) throws SQLException {
        @NotNull final String projectId = findByIndex(userId, index).getId();
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? and id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, projectId);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void changeStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws SQLException {
        @NotNull final String query = "UPDATE " + getTableName() + " SET status = ? WHERE user_id = ? and name = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, status.toString());
        statement.setString(2, userId);
        statement.setString(3, name);
        statement.executeUpdate();
        statement.close();
    }

}
