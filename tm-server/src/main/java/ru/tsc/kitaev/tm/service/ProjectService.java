package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.IProjectService;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.model.Project;
import ru.tsc.kitaev.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectService extends AbstractOwnerService<Project> implements IProjectService {

    public ProjectService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        super(connectionService, logService);
    }

    @NotNull
    public IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.add(userId, project);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.add(userId, project);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
        return project;
    }

    @Override
    public void addAll(@NotNull final List<Project> projects) throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.addAll(projects);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Project findByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            return projectRepository.findByName(userId, name);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.removeByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @NotNull final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.updateById(userId, id, name, description);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @NotNull final String description
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.updateByIndex(userId, index, name, description);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.startById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.startByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void startByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.startByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.finishById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        if (projectRepository.getSize() < index - 1) throw new ProjectNotFoundException();
        try {
            projectRepository.finishByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void finishByName(@Nullable final String userId, @Nullable final String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.finishByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.changeStatusById(userId, id, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.changeStatusByIndex(userId, index, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getRepository(connection);
        try {
            projectRepository.changeStatusByName(userId, name, status);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

}
