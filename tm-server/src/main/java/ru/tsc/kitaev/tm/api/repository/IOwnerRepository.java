package ru.tsc.kitaev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository <E extends AbstractOwnerEntity> extends IRepository<E> {

    void remove(@NotNull final String userId, @NotNull final E entity) throws SQLException;

    void clear(@NotNull final String userId) throws SQLException;

    @NotNull
    List<E> findAll(@NotNull final String userId) throws SQLException;

    @NotNull
    List<E> findAll(@NotNull final String userId, @NotNull final String sort) throws SQLException;

    @Nullable
    E findById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    @NotNull
    E findByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException;

    void removeById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    void removeByIndex(@NotNull final String userId, @NotNull final Integer index) throws SQLException;

    boolean existsById(@NotNull final String userId, @NotNull final String id) throws SQLException;

    boolean existsByIndex(@NotNull final String userId, int index) throws SQLException;

    @NotNull
    Integer getSize(@NotNull final String userId) throws SQLException;

}
