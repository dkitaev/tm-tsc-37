package ru.tsc.kitaev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface IUserEndpoint {

    @Nullable
    @SneakyThrows
    @WebMethod
    User findById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session
    );

    @SneakyThrows
    @WebMethod
    void setPassword(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "password", partName = "password") @Nullable final String password
    );

    @SneakyThrows
    @WebMethod
    void updateUser(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "firstName", partName = "firstName") @NotNull final String firstName,
            @WebParam(name = "lastName", partName = "lastName") @NotNull final String lastName,
            @WebParam(name = "middleName", partName = "middleName") @NotNull final String middleName
    );

}
