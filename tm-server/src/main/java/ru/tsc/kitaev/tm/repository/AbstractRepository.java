package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IRepository;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.model.AbstractEntity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected abstract E fetch(@NotNull final ResultSet row) throws SQLException;

    @NotNull
    public static Predicate<AbstractEntity> predicateById(@NotNull final String id) {
        return s -> id.equals(s.getId());
    }

    @Override
    public void remove(@NotNull final E entity) throws SQLException {
        @NotNull final String id = entity.getId();
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void clear() throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.executeUpdate();
        statement.close();
    }

    @NotNull
    @Override
    public List<E> findAll() throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<E> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public List<E> findAll(@NotNull Comparator<E> comparator) throws SQLException {
        @NotNull final List<E> entitiesList = findAll();
        entitiesList.sort(comparator);
        return entitiesList;
    }

    @NotNull
    @Override
    public E findById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @NotNull
    @Override
    public E findByIndex(@NotNull final Integer index) throws SQLException {
        @NotNull final String query = "SELECT * FROM  " + getTableName() + " LIMIT 1 OFFSET ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setInt(1, index);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) throw new EntityNotFoundException();
        @NotNull final E result = fetch(resultSet);
        statement.close();
        return result;
    }

    @Override
    public void removeById(@NotNull final String id) throws SQLException {
        @NotNull final String query = "DELETE FROM " + getTableName() + " WHERE id = ?";
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        statement.setString(1, id);
        statement.executeUpdate();
        statement.close();
    }

    @Override
    public void removeByIndex(@NotNull final Integer index) throws SQLException {
        @NotNull final String id = findByIndex(index).getId();
        removeById(id);
    }

    @Override
    public boolean existsById(@NotNull final String id) throws SQLException {
        @Nullable final E entity = findById(id);
        return entity != null;
    }

    @Override
    public boolean existsByIndex(final int index) throws SQLException {
        if (index < 0) return false;
        return index < getSize();
    }

    @Override
    public int getSize() throws SQLException {
        @NotNull final String query = "SELECT count(*) AS count FROM " + getTableName();
        @NotNull final PreparedStatement statement = connection.prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        resultSet.next();
        return resultSet.getInt("count");
    }

}
