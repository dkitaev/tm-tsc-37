package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IOwnerRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.IOwnerService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIdException;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.model.AbstractOwnerEntity;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    public AbstractOwnerService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        super(connectionService, logService);
    }

    @NotNull
    protected abstract IOwnerRepository<E> getRepository(@NotNull final Connection connection);

    @Override
    public void remove(@Nullable final String userId, @Nullable final E entity) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new RuntimeException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            ownerRepository.remove(userId, entity);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            ownerRepository.clear(userId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.findAll(userId);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public List<E> findAll(@Nullable final String userId, @Nullable final String sort) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return Collections.emptyList();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.findAll(userId, sort);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Nullable
    @Override
    public E findById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.findById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.findByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            ownerRepository.removeById(userId, id);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            ownerRepository.removeByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) return false;
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.existsById(userId, id);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public boolean existsByIndex(@Nullable final String userId, final int index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) return false;
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.existsByIndex(userId, index);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public Integer getSize(@Nullable final String userId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IOwnerRepository<E> ownerRepository = getRepository(connection);
        try {
            return ownerRepository.getSize(userId);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

}
