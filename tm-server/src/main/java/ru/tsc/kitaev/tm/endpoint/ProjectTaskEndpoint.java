package ru.tsc.kitaev.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.kitaev.tm.api.service.IServiceLocator;
import ru.tsc.kitaev.tm.exception.AbstractException;
import ru.tsc.kitaev.tm.model.Session;
import ru.tsc.kitaev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    @SneakyThrows
    @WebMethod
    public List<Task> findTaskByProjectId(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectTaskService().findTaskByProjectId(session.getUserId(), projectId);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void bindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().bindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void unbindTaskById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId,
            @WebParam(name = "taskId", partName = "taskId") @Nullable final String taskId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().unbindTaskById(session.getUserId(), projectId, taskId);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void removeById(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "projectId", partName = "projectId") @Nullable final String projectId
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().removeById(session.getUserId(), projectId);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void removeByIndex(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "index", partName = "index") @Nullable final Integer index
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().removeByIndex(session.getUserId(), index);
    }

    @Override
    @SneakyThrows
    @WebMethod
    public void removeByName(
            @WebParam(name = "session", partName = "session") @Nullable final Session session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) throws AbstractException {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectTaskService().removeByName(session.getUserId(), name);
    }

}
