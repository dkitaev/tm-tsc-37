package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IUserRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.IPropertyService;
import ru.tsc.kitaev.tm.api.service.IUserService;
import ru.tsc.kitaev.tm.enumerated.Role;
import ru.tsc.kitaev.tm.exception.empty.*;
import ru.tsc.kitaev.tm.exception.entity.EntityNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.exception.user.EmailExistsException;
import ru.tsc.kitaev.tm.exception.user.LoginExistsException;
import ru.tsc.kitaev.tm.model.User;
import ru.tsc.kitaev.tm.repository.UserRepository;
import ru.tsc.kitaev.tm.util.HashUtil;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService,
            @NotNull final IPropertyService propertyService
    ) {
        super(connectionService, logService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull Connection connection) {
        return new UserRepository(connection);
    }

    @Override
    public void addAll(@NotNull final List<User> users) throws SQLException {
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.addAll(users);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            return userRepository.findByLogin(login);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void isLoginExists(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            if (userRepository.isLoginExists(login)) throw new LoginExistsException();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws SQLException {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            return userRepository.findByEmail(email);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void isEmailExists(final String email) throws SQLException {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            if (userRepository.isEmailExists(email)) throw new EmailExistsException();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeUser(@Nullable final User user) throws SQLException {
        if (user == null) throw new EntityNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.removeUser(user);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.removeByLogin(login);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) throws SQLException {
        isLoginExists(login);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.add(user);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws SQLException {
        isLoginExists(login);
        isEmailExists(email);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        setPassword(user, password);
        user.setEmail(email);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.add(user);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws SQLException {
        isLoginExists(login);
        if (role == null) throw new EmptyRoleException();
        @NotNull final User user = new User();
        user.setLogin(login);
        setPassword(user, password);
        user.setRole(role);
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.add(user);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
        return user;
    }

    @Override
    public void setPassword(@Nullable final String userId, @Nullable final String password) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        @Nullable final User user = findById(userId);
        if (user == null) throw new EntityNotFoundException();
        setPassword(user, password);
        try {
            userRepository.updatePassword(userId, user.getPasswordHash());
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void setPassword(@Nullable final User user, @Nullable final String password) {
        if (user == null) throw new EntityNotFoundException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final Integer iteration = propertyService.getPasswordIteration();
        @NotNull final String hash = HashUtil.salt(secret, iteration, password);
        user.setPasswordHash(hash);
    }

    @Override
    public void updateUser(
            @Nullable final String userId,
            @NotNull final String firstName,
            @NotNull final String lastName,
            @NotNull final String middleName
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.updateUser(userId, firstName, lastName, middleName);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.updateLockUserByLogin(login, true);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws SQLException {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IUserRepository userRepository = getRepository(connection);
        try {
            userRepository.updateLockUserByLogin(login, false);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

}
