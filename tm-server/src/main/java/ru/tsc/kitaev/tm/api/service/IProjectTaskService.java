package ru.tsc.kitaev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface IProjectTaskService {

    @NotNull
    List<Task> findTaskByProjectId(@Nullable String userId, @Nullable String projectId) throws SQLException;

    void bindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws SQLException;

    void unbindTaskById(@Nullable String userId, @Nullable String projectId, @Nullable String taskId) throws SQLException;

    void removeById(@Nullable String userId, @Nullable String projectId) throws SQLException;

    void removeByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException;

    void removeByName(@Nullable String userId, @Nullable String name) throws SQLException;

}
