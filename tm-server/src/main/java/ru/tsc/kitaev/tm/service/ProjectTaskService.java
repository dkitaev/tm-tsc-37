package ru.tsc.kitaev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.api.repository.ITaskRepository;
import ru.tsc.kitaev.tm.api.service.IConnectionService;
import ru.tsc.kitaev.tm.api.service.ILogService;
import ru.tsc.kitaev.tm.api.service.IProjectTaskService;
import ru.tsc.kitaev.tm.exception.empty.EmptyIndexException;
import ru.tsc.kitaev.tm.exception.empty.EmptyUserIdException;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.kitaev.tm.exception.system.DatabaseOperationException;
import ru.tsc.kitaev.tm.model.Task;
import ru.tsc.kitaev.tm.repository.ProjectRepository;
import ru.tsc.kitaev.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final ILogService logService;

    public ProjectTaskService(
            @NotNull final IConnectionService connectionService,
            @NotNull final ILogService logService
    ) {
        this.connectionService = connectionService;
        this.logService = logService;
    }

    @NotNull
    public IProjectRepository getProjectRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    public ITaskRepository getTaskRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    @Override
    public List<Task> findTaskByProjectId(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        try {
            return taskRepository.findAllTaskByProjectId(userId, projectId);
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void bindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
        try {
            taskRepository.bindTaskToProjectById(userId, projectId, taskId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void unbindTaskById(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        if (taskId == null || taskId.isEmpty()) throw new TaskNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        if (!taskRepository.existsById(taskId)) throw new TaskNotFoundException();
        try {
            taskRepository.unbindTaskById(userId, projectId, taskId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String projectId) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        try {
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByIndex(@Nullable String userId, @Nullable Integer index) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        try {
            @NotNull final String projectId = projectRepository.findByIndex(userId, index).getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByIndex(userId, index);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

    @Override
    public void removeByName(@Nullable String userId, @Nullable String name) throws SQLException {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final Connection connection = connectionService.getConnection();
        @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
        @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
        try {
            @NotNull final String projectId = projectRepository.findByName(userId, name).getId();
            taskRepository.removeAllTaskByProjectId(userId, projectId);
            projectRepository.removeByName(userId, name);
            connection.commit();
        } catch (SQLException e) {
            logService.error(e);
            throw new DatabaseOperationException();
        } finally {
            connection.close();
        }
    }

}
